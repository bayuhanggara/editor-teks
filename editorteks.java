/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package editorteks;

/**
 *
 * @author BAYU HANGGARA
 */
import java.awt.*;
import java.awt.event.*;
import java.io.*;
public class EditorTeks extends Frame {
    String namaFile = "" ;
    TextArea tArea;
    MenuItem itemBuka;
    MenuItem itemSimpan;
    MenuItem itemKeluar;
    MenuItem itemPutih;
    MenuItem itemAbuAbu;
    MenuItem itemHitam;
    MenuItem itemBiru;
    


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
   EditorTeks apl = new EditorTeks();
    }
    
     public EditorTeks(){
        super ("Editor Teks Sederhana");
        setSize (400,250);
        
        MenuBar menuApl = new MenuBar();
        
        setMenuBar (menuApl);
        
       
        
        Button close = new Button("Close");
            Panel p = new Panel();
            p.add(close);
            
            add("South", p);
            
        close.addActionListener(
            new EditorTeks.PenanganTombol());
    
        addWindowListener(
            new EditorTeks.PenanganKejadian());
        
        Menu menuFile = new Menu ("File");
        Menu menuWarna = new Menu ("Warna");
        menuApl.add(menuFile);
        menuApl.add(menuWarna);
        
        MenuItem itemGaris;
        
        itemBuka = new MenuItem("Buka...");
        itemSimpan = new MenuItem("Simpan");
        itemGaris = new MenuItem("_");
        itemKeluar = new MenuItem("Keluar");
        
        menuFile.add(itemBuka);
        menuFile.add(itemSimpan);
        menuFile.add(itemGaris);
        menuFile.add(itemKeluar);
        
        Menu submenuWarnaLB = new Menu ("Warna Latar Belakang");
        Menu submenuWarnaTeks = new Menu ("Warna Teks");
        
        menuWarna.add(submenuWarnaLB);
        menuWarna.add(submenuWarnaTeks);
        
        MenuItem itemPutih = new MenuItem("Putih");
        MenuItem itemAbuAbu = new MenuItem("Abu-Abu");
        MenuItem itemKuning = new MenuItem("Kuning");
      
        submenuWarnaLB.add (itemPutih);
        submenuWarnaLB.add (itemAbuAbu);
        submenuWarnaLB.add (itemKuning);
       
       
        MenuItem itemHitam = new MenuItem ("Hitam");
        MenuItem itemBiru = new MenuItem ("Biru");
        MenuItem itemMerah = new MenuItem ("Merah");
        
        submenuWarnaTeks.add(itemHitam);
        submenuWarnaTeks.add(itemBiru);
        submenuWarnaTeks.add(itemMerah);
        
        tArea = new TextArea("");
        tArea.setFont (new Font ("monospaced",Font.PLAIN, 25));
        add (tArea);
        
        itemBuka.addActionListener (
            new EditorTeks.PenanganItemMenu());
        itemSimpan.addActionListener (
            new EditorTeks.PenanganItemMenu());
        itemKeluar.addActionListener (
            new EditorTeks.PenanganItemMenu());
        itemPutih.addActionListener (
            new EditorTeks.PenanganItemMenu());
        itemAbuAbu.addActionListener (
            new EditorTeks.PenanganItemMenu());
        itemHitam.addActionListener (
            new EditorTeks.PenanganItemMenu());
          itemKuning.addActionListener (
            new EditorTeks.PenanganItemMenu());
        itemBiru.addActionListener (
            new EditorTeks.PenanganItemMenu());
        itemMerah.addActionListener (
            new EditorTeks.PenanganItemMenu());
        show();
    }
 class PenanganTombol implements ActionListener{
     public void actionPerformed(ActionEvent e){
     String a = e.getActionCommand();
     
     if (a.equals("Close")){
         System.exit(0);
     }
     }
    }
    
    class PenanganKejadian extends WindowAdapter{
    public void windowClosing(WindowEvent e){
     dispose();       
            } 
    }   
    public void buka(){
        FileDialog fd = new FileDialog(
                                this, "Buka Berkas");
        fd.show();
        
        this.namaFile = fd.getFile();
        String namaDir = fd.getDirectory();
        this.namaFile = namaDir + this.namaFile;
        if (this.namaFile == null)
            return;
        
        FileReader berkasMasukan = null;
        String data = "";
        
        try {
            berkasMasukan =
                    new FileReader(this.namaFile);
            
            BufferedReader streamMasukan =
                    new BufferedReader (berkasMasukan);
            
            while (true) {
                String barisData =
                        streamMasukan.readLine();
                
                if (barisData == null)
                    break;
                data = data + barisData + "\n";
            }
            berkasMasukan.close();
        }
        catch (IOException i){
                System.err.println(i.getMessage());
    }
    
    tArea.setText(data);
    
}
public void simpan() {
    try {
        FileWriter berkasKeluaran =
                new FileWriter(this.namaFile);
        String isiTextArea = tArea.getText();
        berkasKeluaran.write(isiTextArea);
        berkasKeluaran.flush();
        berkasKeluaran.close();
    }
    catch (IOException i) {
        System.err.println(i.getMessage());
    }
}

class PenanganItemMenu implements ActionListener {
    public void actionPerformed (ActionEvent e) {
        String perintah = e.getActionCommand ();
        if (perintah.equals ("Buka..."))
            buka();
        else if (perintah.equals ( "Simpan"))
            simpan();
        else if (perintah.equals ("Keluar"))
            dispose();
        else if (perintah.equals ("Putih"))
            tArea.setBackground(Color.WHITE);
        else if (perintah.equals ("Abu-Abu"))
            tArea.setBackground(Color.GRAY);
          else if (perintah.equals ("Kuning"))
            tArea.setBackground(Color.YELLOW);
        else if (perintah.equals ("Hitam"))
            tArea.setForeground(Color.BLACK);
        else if (perintah.equals ("Biru"))
            tArea.setForeground(Color.BLUE);
           else if (perintah.equals ("Merah"))
            tArea.setForeground(Color.RED);
        
        }           
    }

}